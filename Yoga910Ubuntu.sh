#*********************************
# WIFI
# Apparently due to a bug
#*********************************

echo "blacklist ideapad-laptop" | sudo tee /etc/modprobe.d/ideapad-laptop.conf
sudo reboot

#*********************************
# Login with fingerprints
#*********************************
sudo apt-add-repository ppa:fingerprint/fingerprint-gui && sudo apt-get update
sudo apt-get install libbsapi policykit-1-fingerprint-gui fingerprint-gui

#*********************************
# Spotify
#*********************************
alias spotify="/usr/bin/spotify --force-device-scale-factor=1.5"
